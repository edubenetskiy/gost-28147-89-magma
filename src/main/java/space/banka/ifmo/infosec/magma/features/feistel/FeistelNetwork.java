package space.banka.ifmo.infosec.magma.features.feistel;

public interface FeistelNetwork {

        long encrypt(long inputBlock, int roundCount, int[] roundKeys);

        long decrypt(long inputBlock, int roundCount, int[] roundKeys);

}
