package space.banka.ifmo.infosec.magma.features.cipher.modes;

import space.banka.ifmo.infosec.magma.features.cipher.BlockCipher;
import space.banka.ifmo.infosec.magma.features.cipher.BlockCipherMode;

import java.nio.LongBuffer;

public class CipherBlockChainingMode implements BlockCipherMode {

    private final long initializationVector;
    private final BlockCipher blockCipher;

    public CipherBlockChainingMode(long initializationVector, BlockCipher blockCipher) {
        this.initializationVector = initializationVector;
        this.blockCipher = blockCipher;
    }

    @Override
    public long[] encrypt(long[] plaintextBlocks, byte[] key) {
        LongBuffer plaintext = LongBuffer.wrap(plaintextBlocks);
        LongBuffer encrypted = LongBuffer.allocate(plaintextBlocks.length);
        long encryptedBlock = initializationVector;
        while (plaintext.hasRemaining()) {
            long plaintextBlock = plaintext.get();
            encryptedBlock = blockCipher.encrypt(key, plaintextBlock ^ encryptedBlock);
            encrypted.put(encryptedBlock);
        }
        return encrypted.array();
    }

    @Override
    public long[] decrypt(long[] ciphertextBlocks, byte[] key) {
        LongBuffer ciphertext = LongBuffer.wrap(ciphertextBlocks);
        LongBuffer decrypted = LongBuffer.allocate(ciphertextBlocks.length);
        long previousCiphertextBlock = initializationVector;
        while (ciphertext.hasRemaining()) {
            long ciphertextBlock = ciphertext.get();
            long decryptedBlock = previousCiphertextBlock ^ blockCipher.decrypt(key, ciphertextBlock);
            decrypted.put(decryptedBlock);
            previousCiphertextBlock = ciphertextBlock;
        }
        return decrypted.array();
    }
}
