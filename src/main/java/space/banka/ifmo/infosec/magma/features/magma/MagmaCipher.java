package space.banka.ifmo.infosec.magma.features.magma;

import space.banka.ifmo.infosec.magma.features.cipher.BlockCipher;
import space.banka.ifmo.infosec.magma.features.feistel.FeistelNetwork;
import space.banka.ifmo.infosec.magma.features.feistel.FeistelNetworkImpl;
import space.banka.ifmo.infosec.magma.features.substitution.SubstitutionBlock;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class MagmaCipher implements BlockCipher {

    public static final int ROUND_COUNT = 32;
    public static final int KEY_LENGTH_BYTES = 32;
    public static final int SUBKEYS_COUNT = 8;
    private final FeistelNetwork feistelNetwork;

    public MagmaCipher(SubstitutionBlock substitutionBlock) {
        feistelNetwork = new FeistelNetworkImpl(new MagmaFunction(substitutionBlock));
    }

    @Override
    public long encrypt(byte[] key, long block) {
        validateKeyLength(key);
        int[] roundKeys = generateRoundKeys(key);
        return feistelNetwork.encrypt(block, ROUND_COUNT, roundKeys);
    }

    @Override
    public long decrypt(byte[] key, long block) {
        validateKeyLength(key);
        int[] roundKeys = generateRoundKeys(key);
        return feistelNetwork.decrypt(block, ROUND_COUNT, roundKeys);
    }

    private void validateKeyLength(byte[] key) {
        if (key.length != KEY_LENGTH_BYTES) {
            throw new IllegalArgumentException("Invalid key length: must be " + KEY_LENGTH_BYTES + ", but given " + key.length);
        }
    }

    private int[] generateRoundKeys(byte[] key) {
        IntBuffer subKeys = ByteBuffer.wrap(key).asIntBuffer();
        assert subKeys.limit() == SUBKEYS_COUNT;
        IntBuffer roundKeys = IntBuffer.allocate(ROUND_COUNT);
        for (int i = 0; i < 24; i++) {
            roundKeys.put(i, subKeys.get(i % SUBKEYS_COUNT));
        }
        for (int i = 24; i < ROUND_COUNT; i++) {
            roundKeys.put(i, subKeys.get(SUBKEYS_COUNT - (i % SUBKEYS_COUNT) - 1));
        }
        return roundKeys.array();
    }
}
