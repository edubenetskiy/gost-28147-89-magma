package space.banka.ifmo.infosec.magma.features.feistel;

public class FeistelNetworkImpl implements FeistelNetwork {

    private FeistelCell cell;

    public FeistelNetworkImpl(FeistelCell cell) {
        this.cell = cell;
    }

    @Override
    public long encrypt(long inputBlock, int roundCount, int[] roundKeys) {
        validateRoundCount(roundCount);
        validateNumberOfRoundKeys(roundCount, roundKeys);
        int prevLeftHalf = (int) (inputBlock >> Integer.SIZE);
        int prevRightHalf = (int) inputBlock;
        int leftHalf = 0;
        int rightHalf = 0;
        for (int i = 0; i < roundCount; i++) {
            leftHalf = prevRightHalf;
            rightHalf = prevLeftHalf ^ cell.apply(prevRightHalf, roundKeys[i]);

            prevLeftHalf = leftHalf;
            prevRightHalf = rightHalf;
        }
        long outBlock = (((long) rightHalf) << Integer.SIZE) | (leftHalf & 0xffff_ffffL);
        return outBlock;
    }

    @Override
    public long decrypt(long inputBlock, int roundCount, int[] roundKeys) {
        validateRoundCount(roundCount);
        validateNumberOfRoundKeys(roundCount, roundKeys);

        int prevLeftHalf = (int) (inputBlock >> Integer.SIZE);
        int prevRightHalf = (int) inputBlock;

        int leftHalf = 0;
        int rightHalf = 0;
        for (int i = roundCount - 1; i >= 0; i--) {
            leftHalf = prevRightHalf;
            rightHalf = prevLeftHalf ^ cell.apply(prevRightHalf, roundKeys[i]);

            prevLeftHalf = leftHalf;
            prevRightHalf = rightHalf;
        }
        long outBlock = (((long) rightHalf) << Integer.SIZE) | (leftHalf & 0xffff_ffffL);
        return outBlock;
    }

    private void validateRoundCount(int roundCount) {
        if (roundCount <= 0) {
            throw new IllegalArgumentException("Number of rounds must to be greater than 0.");
        }
    }

    private void validateNumberOfRoundKeys(int roundCount, int[] roundKeys) {
        if (roundKeys.length != roundCount) {
            throw new IllegalArgumentException("Number of round keys must be equal to round count");
        }
    }
}
