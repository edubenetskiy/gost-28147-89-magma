package space.banka.ifmo.infosec.magma.features.padding;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;

public class PKCS7Padding implements Padding {

    @Override
    public long[] padAndSplit(byte[] plaintextBytes) {
        int paddingLength = Long.BYTES - (plaintextBytes.length % Long.BYTES);
        int totalLength = plaintextBytes.length + paddingLength;
        assert totalLength % Long.BYTES == 0;

        ByteBuffer bytesWithPadding = ByteBuffer.allocate(totalLength);
        bytesWithPadding.put(plaintextBytes);
        for (int i = 0; i < paddingLength; i++) {
            bytesWithPadding.put((byte) paddingLength);
        }
        assert bytesWithPadding.position() == bytesWithPadding.limit();

        return LongBuffer
                .allocate(totalLength / Long.BYTES)
                .put(bytesWithPadding.rewind().asLongBuffer())
                .array();
    }

    @Override
    public byte[] assembleAndClip(long[] ciphertextBlocks) {
        ByteBuffer bytes = ByteBuffer.allocate(ciphertextBlocks.length * Long.BYTES);
        bytes.asLongBuffer().put(ciphertextBlocks);
        byte paddingLength = bytes.rewind().get(bytes.limit() - 1);
        int lengthWithoutPadding = bytes.limit() - paddingLength;
        return ByteBuffer
                .allocate(lengthWithoutPadding)
                .put(bytes.array(), 0, lengthWithoutPadding)
                .array();
    }
}
