package space.banka.ifmo.infosec.magma.features.cipher;

public interface BlockCipher {

    long encrypt(byte[] key, long block);

    long decrypt(byte[] key, long block);

}
