package space.banka.ifmo.infosec.magma.features.substitution;

public class SubstitutionBlockImpl implements SubstitutionBlock {

    public static final int NUM_ENTRIES = 16;
    private static final int MIN_VALUE = 0;
    private static final int MAX_VALUE = NUM_ENTRIES - 1;
    private final int[] substitutions;

    public SubstitutionBlockImpl(int[] substitutions) {
        validateTableOfSubstitutions(substitutions);
        this.substitutions = substitutions;
    }

    @Override
    public int substitute(int sequence) {
        if (sequence < MIN_VALUE || sequence > MAX_VALUE) {
            throw new IllegalArgumentException(
                    "Substitutions only defined for values " +
                    "between " + MIN_VALUE + " and " + MAX_VALUE + ", " +
                    "but given " + sequence);
        }
        return substitutions[sequence];
    }

    private void validateTableOfSubstitutions(int[] substitutions) {
        if (substitutions.length != NUM_ENTRIES) {
            throw new IllegalArgumentException(
                    "Table of substitutions must have exactly " + NUM_ENTRIES +
                    " entries, but given " + substitutions.length + " entries.");
        }
        for (int i = 0; i < NUM_ENTRIES; i++) {
            if (substitutions[i] < MIN_VALUE || substitutions[i] > MAX_VALUE) {
                throw new IllegalArgumentException(
                        "Invalid substitution at position " + i + ": " +
                        "should be between " + MIN_VALUE + " and " + MAX_VALUE + ", " +
                        "but is" + substitutions[i]);
            }
        }
    }
}
