package space.banka.ifmo.infosec.magma.features.substitution;

public interface SubstitutionBlock {

    int substitute(int sequence);
}
