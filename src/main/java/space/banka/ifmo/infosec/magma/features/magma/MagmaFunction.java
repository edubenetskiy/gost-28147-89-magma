package space.banka.ifmo.infosec.magma.features.magma;

import space.banka.ifmo.infosec.magma.features.feistel.FeistelCell;
import space.banka.ifmo.infosec.magma.features.substitution.SubstitutionBlock;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class MagmaFunction implements FeistelCell {

    public static final int NUM_TETRADES = 8;
    public static final int LEFT_SHIFT_DISTANCE = 11;

    public static final int TETRADE_MASK = 0x0F;
    public static final int TETRADE_SIZE = 4;
    public static final int TETRADES_PER_BYTE = 2;

    private SubstitutionBlock[] substitutionBlocks = new SubstitutionBlock[NUM_TETRADES];

    public MagmaFunction(SubstitutionBlock substitutionBlock) {
        Arrays.fill(this.substitutionBlocks, substitutionBlock);
    }

    @Override
    public int apply(int block, int roundKey) {
        int sumMod32 = block + roundKey;
        int[] tetrades = splitToTetrades(sumMod32);

        for (int tetradeIndex = 0; tetradeIndex < tetrades.length; tetradeIndex++) {
            tetrades[tetradeIndex] = this.substitutionBlocks[tetradeIndex].substitute(tetrades[tetradeIndex]);
        }

        int output = assembleTetrades(tetrades);
        return Integer.rotateLeft(output, LEFT_SHIFT_DISTANCE);
    }

    private int[] splitToTetrades(int block) {
        byte[] bytes = ByteBuffer.allocate(Integer.BYTES).putInt(block).array();
        int[] tetrades = new int[Integer.BYTES * TETRADES_PER_BYTE];
        for (int tetradeIndex = 0; tetradeIndex < tetrades.length; tetradeIndex = tetradeIndex + TETRADES_PER_BYTE) {
            tetrades[tetradeIndex] = (bytes[tetradeIndex / TETRADES_PER_BYTE] >> TETRADE_SIZE) & TETRADE_MASK;
            tetrades[tetradeIndex + 1] = (bytes[tetradeIndex / TETRADES_PER_BYTE] & TETRADE_MASK);
        }
        return tetrades;
    }

    private int assembleTetrades(int[] tetrades) {
        byte[] bytes = new byte[tetrades.length / TETRADES_PER_BYTE];
        for (int tetradeIndex = 0; tetradeIndex < tetrades.length; tetradeIndex += TETRADES_PER_BYTE) {
            int higherTetrade = tetrades[tetradeIndex] & TETRADE_MASK;
            int lowerTetrade = tetrades[tetradeIndex + 1] & TETRADE_MASK;
            bytes[tetradeIndex / TETRADES_PER_BYTE] = (byte) ((higherTetrade << TETRADE_SIZE) | lowerTetrade);
        }
        return ByteBuffer.wrap(bytes).getInt();
    }
}
