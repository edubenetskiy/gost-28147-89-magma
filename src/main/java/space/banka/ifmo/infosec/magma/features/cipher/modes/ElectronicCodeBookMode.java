package space.banka.ifmo.infosec.magma.features.cipher.modes;

import space.banka.ifmo.infosec.magma.features.cipher.BlockCipher;
import space.banka.ifmo.infosec.magma.features.cipher.BlockCipherMode;

import java.nio.LongBuffer;

public class ElectronicCodeBookMode implements BlockCipherMode {

    private final BlockCipher blockCipher;

    public ElectronicCodeBookMode(BlockCipher blockCipher) {
        this.blockCipher = blockCipher;
    }

    @Override
    public long[] encrypt(long[] plaintextBlocks, byte[] key) {
        LongBuffer plaintext = LongBuffer.wrap(plaintextBlocks);
        LongBuffer encrypted = LongBuffer.allocate(plaintextBlocks.length);
        while (plaintext.hasRemaining()) {
            long plaintextBlock = plaintext.get();
            long encryptedBlock = blockCipher.encrypt(key, plaintextBlock);
            encrypted.put(encryptedBlock);
        }
        return encrypted.array();
    }

    @Override
    public long[] decrypt(long[] ciphertextBytes, byte[] key) {
        LongBuffer ciphertext = LongBuffer.wrap(ciphertextBytes);
        LongBuffer decrypted = LongBuffer.allocate(ciphertextBytes.length);
        while (ciphertext.hasRemaining()) {
            long ciphertextBlock = ciphertext.get();
            long decryptedBlock = blockCipher.decrypt(key, ciphertextBlock);
            decrypted.put(decryptedBlock);
        }
        return decrypted.array();
    }
}
