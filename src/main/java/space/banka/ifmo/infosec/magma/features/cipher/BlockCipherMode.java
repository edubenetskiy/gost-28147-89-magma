package space.banka.ifmo.infosec.magma.features.cipher;

public interface BlockCipherMode {

    long[] encrypt(long[] plaintextBlocks, byte[] key);

    long[] decrypt(long[] ciphertextBlocks, byte[] key);
}
