package space.banka.ifmo.infosec.magma.features.feistel;

/**
 * A round function for {@link FeistelNetwork}.
 *
 * <p>{@link FeistelNetwork} uses {@link FeistelCell} internally
 * to transform semi-blocks on each round.
 */
public interface FeistelCell {

    /**
     * Compute a 32-bit output semi-block
     * from the provided 32-bit input semi-block
     * using the given round key.
     *
     * @param block    a 32-bit input semi-block
     * @param roundKey the round key to use in the transformation
     * @return the 32-bit output semi-block
     */
    int apply(int block, int roundKey);

}
