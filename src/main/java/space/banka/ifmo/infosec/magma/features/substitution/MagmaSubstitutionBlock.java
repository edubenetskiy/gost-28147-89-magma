package space.banka.ifmo.infosec.magma.features.substitution;

public enum MagmaSubstitutionBlock implements SubstitutionBlock {
    ID_GOST_28147_89_CRYPTOPRO_A_PARAM_SET_1(new int[]{
            0x9, 0x6, 0x3, 0x2, 0x8, 0xB, 0x1, 0x7,
            0xA, 0x4, 0xE, 0xF, 0xC, 0x0, 0xD, 0x5,
    });

    private final SubstitutionBlockImpl inner;

    MagmaSubstitutionBlock(int[] substitutions) {
        this.inner = new SubstitutionBlockImpl(substitutions);
    }

    @Override
    public int substitute(int sequence) {
        return inner.substitute(sequence);
    }
}
