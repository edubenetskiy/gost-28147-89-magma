package space.banka.ifmo.infosec.magma.features.padding;

public interface Padding {

    long[] padAndSplit(byte[] plaintextBytes);

    byte[] assembleAndClip(long[] ciphertextBlocks);
}
