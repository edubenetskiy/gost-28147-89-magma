package space.banka.ifmo.infosec.magma.features.cipher.modes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import space.banka.ifmo.infosec.magma.features.cipher.BlockCipher;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ElectronicCodeBookModeTest {

    private Random random;
    private ElectronicCodeBookMode subject;
    private long initializationVector;

    @BeforeEach
    void setUp() {
        random = new Random();
        initializationVector = random.nextLong();
        BlockCipher blockCipher = new FakeBlockCipher();
        subject = new ElectronicCodeBookMode(blockCipher);
    }

    @Test
    void decryption_of_encrypted_renders_original_message() {
        byte[] key = new byte[32];
        random.nextBytes(key);

        String plaintext = "Hello! The length of this message must be a multiple of 64 bits.";
        byte[] plaintextBytes = plaintext.getBytes();
        long[] plaintextBlocks = LongBuffer
                .allocate(plaintextBytes.length / Long.BYTES)
                .put(ByteBuffer.wrap(plaintextBytes).asLongBuffer())
                .array();

        long[] encryptedBlocks = subject.encrypt(plaintextBlocks, key);
        long[] decryptedBlocks = subject.decrypt(encryptedBlocks, key);

        ByteBuffer decryptedBytes = ByteBuffer.allocate(decryptedBlocks.length * Long.BYTES);
        decryptedBytes.asLongBuffer().put(decryptedBlocks);
        String decryptedText = new String(decryptedBytes.array());

        assertEquals(plaintext, decryptedText);
    }
}
