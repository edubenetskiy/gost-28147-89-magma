package space.banka.ifmo.infosec.magma.features.feistel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FeistelNetworkImplTest {

    private FeistelNetworkImpl feistelNetwork;
    private Random random;

    @BeforeEach
    void setUp() {
        FeistelCell feistelCell = (block, roundKey) -> block + roundKey + 12345;
        feistelNetwork = new FeistelNetworkImpl(feistelCell);
        random = new Random();
    }

    @Test
    void decryption_of_encrypted_renders_original_value() {
        long originalBlock = random.nextLong();
        int numRounds = 32;
        int[] roundKeys = new int[numRounds];
        for (int i = 0; i < roundKeys.length; i++) {
            roundKeys[i] = random.nextInt();
        }
        long encryptedBlock = feistelNetwork.encrypt(originalBlock, numRounds, roundKeys);
        long decryptedBlock = feistelNetwork.decrypt(encryptedBlock, numRounds, roundKeys);
        assertEquals(originalBlock, decryptedBlock);
    }
}
