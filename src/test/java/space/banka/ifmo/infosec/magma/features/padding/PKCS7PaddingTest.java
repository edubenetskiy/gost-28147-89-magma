package space.banka.ifmo.infosec.magma.features.padding;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PKCS7PaddingTest {

    @Test
    void assemble_disassembled_renders_original() {
        PKCS7Padding subject = new PKCS7Padding();
        byte[] bytes = "This is an exemplary message.".getBytes();
        assertArrayEquals(bytes, subject.assembleAndClip(subject.padAndSplit(bytes)));
    }
}
