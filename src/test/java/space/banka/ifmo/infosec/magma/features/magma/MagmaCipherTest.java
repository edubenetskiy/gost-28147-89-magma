package space.banka.ifmo.infosec.magma.features.magma;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import space.banka.ifmo.infosec.magma.features.substitution.SubstitutionBlock;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MagmaCipherTest {

    private MagmaCipher subject;
    private Random random;

    @BeforeEach
    void setUp() {
        SubstitutionBlock substitutionBlock = sequence -> 15 - sequence;
        this.subject = new MagmaCipher(substitutionBlock);
        this.random = new Random();
    }

    @Test
    void decrypting_encrypted_renders_plaintext() {
        byte[] key = randomKey();
        long plaintextBlock = random.nextLong();
        long encryptedBlock = subject.encrypt(key, plaintextBlock);
        long decryptedBlock = subject.decrypt(key, encryptedBlock);
        assertEquals(plaintextBlock, decryptedBlock);
    }

    private byte[] randomKey() {
        byte[] key = new byte[32];
        new Random().nextBytes(key);
        return key;
    }
}
