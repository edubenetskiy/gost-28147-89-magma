package space.banka.ifmo.infosec.magma;

import org.junit.jupiter.api.Test;
import space.banka.ifmo.infosec.magma.features.feistel.FeistelCell;
import space.banka.ifmo.infosec.magma.features.feistel.FeistelNetworkImpl;



public class FeistelCellTest {

    @Test
    void blockSplitTest() {
        long block = 0xDEADBEEFFEEBDAEDL;
        int[] keys = {1,2};
        FeistelCell cell = new FeistelCell() {
            @Override
            public int apply(int block, int roundKey) {
                return 0;
            }
        };
        FeistelNetworkImpl imp = new FeistelNetworkImpl(cell);
        imp.encrypt(block, 2, keys);


    }
}
