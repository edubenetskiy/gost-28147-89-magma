package space.banka.ifmo.infosec.magma.features.cipher.modes;

import space.banka.ifmo.infosec.magma.features.cipher.BlockCipher;

class FakeBlockCipher implements BlockCipher {

    @Override
    public long encrypt(byte[] key, long block) {
        long salt = computeSalt(key);
        return block ^ salt;
    }

    @Override
    public long decrypt(byte[] key, long block) {
        long salt = computeSalt(key);
        return block ^ salt;
    }

    private long computeSalt(byte[] key) {
        long salt = 0;
        for (byte k : key) {
            salt += k;
            salt <<= 1;
        }
        return salt;
    }
}
